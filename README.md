This is a test application for [Spring Selfedu](https://bitbucket.org/piotrekag/spring_selfedu) app.

The application contains integration tests which test API calls using Rest Assured test framework.

The quickest way to run it is:

 1. Clone the Spring Selfedu repo via HTTP:

        git clone https://piotrekag@bitbucket.org/piotrekag/spring_selfedu.git

    or SSH:

        git clone git@bitbucket.org:piotrekag/spring_selfedu.git

 2. Run:

        cd <spring_selfedu_project_root>
        ./mvnw clean install
        docker-compose up

    (Requires docker and docker-compose.)

 3. Run test command:

        cd <rest_assured_test_project_root>
        ./mvnw clean test
