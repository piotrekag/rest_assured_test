package selfedu.spring.rest_assured_test;

import io.restassured.response.Response;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UsersEndpointTest {

    private final String CONTEXT_PATH="/springselfedu";
    private final String EMAIL_ADDRESS = "realtestaccount@piotrekag.awsapps.com";
    private final String JSON = "application/json";
    private static String authorizationHeader;
    private static String userId;
    private static List<Map<String, String>> addresses;

    @BeforeEach
    void setUp() throws Exception { }

    @BeforeAll
    static void verifyEmailAddress() {
        updateUserEmailVerificationStatus();
    }

    static void updateUserEmailVerificationStatus() {

        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/spring-selfedu-db", "postgres", "postgres")) {
            System.out.println("Connected to PostgreSQL database!");
            Statement statement = connection.createStatement();
            statement.executeUpdate("UPDATE users SET email_verification_status='true' WHERE email='realtestaccount@piotrekag.awsapps.com';");
        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }

    }

    // test user login
    @Test
    final void a() {

        Map<String, String> loginDetails = new HashMap<>();
        loginDetails.put("email", EMAIL_ADDRESS);
        loginDetails.put("password", "123");

        Response resposne = given().
                contentType(JSON).
                accept(JSON).
                body(loginDetails).
                when().
                post(CONTEXT_PATH + "/users/login").
                then().
                statusCode(200).extract().response();

        authorizationHeader = resposne.header("Authorization");
        userId = resposne.header("UserId");

        assertNotNull(authorizationHeader);
        assertNotNull(userId);

    }

    // test get user details
    @Test
    final void b() {

        Response response = given()
                .pathParam("id", userId)
                .header("Authorization",authorizationHeader)
                .accept(JSON)
                .when()
                .get(CONTEXT_PATH + "/users/{id}")
                .then()
                .statusCode(200)
                .contentType(JSON)
                .extract()
                .response();

        String userPublicId = response.jsonPath().getString("userId");
        String userEmail = response.jsonPath().getString("email");
        String firstName = response.jsonPath().getString("firstName");
        String lastName = response.jsonPath().getString("lastName");
        addresses = response.jsonPath().getList("addresses");
        String addressId = addresses.get(0).get("addressId");

        assertNotNull(userPublicId);
        assertNotNull(userEmail);
        assertNotNull(firstName);
        assertNotNull(lastName);
        assertEquals(EMAIL_ADDRESS, userEmail);

        assertTrue(addresses.size() == 2);
        assertTrue(addressId.length() == 30);

    }

    // test update user details
    @Test
    final void c() {

        Map<String, Object> userDetails = new HashMap<>();
        userDetails.put("firstName", "John");
        userDetails.put("lastName", "Doe");

        Response response = given()
                .contentType(JSON)
                .accept(JSON)
                .header("Authorization",authorizationHeader)
                .pathParam("id", userId)
                .body(userDetails)
                .when()
                .put(CONTEXT_PATH + "/users/{id}")
                .then()
                .statusCode(200)
                .contentType(JSON)
                .extract()
                .response();

        String firstName = response.jsonPath().getString("firstName");
        String lastName = response.jsonPath().getString("lastName");

        List<Map<String, String>> storedAddresses = response.jsonPath().getList("addresses");

        assertEquals("John", firstName);
        assertEquals("Doe", lastName);
        assertNotNull(storedAddresses);
        assertTrue(addresses.size() == storedAddresses.size());
        assertEquals(addresses.get(0).get("streetName"), storedAddresses.get(0).get("streetName"));
    }

    // test delete user details
    @Test
    final void d() {

        Response response = given()
                .header("Authorization",authorizationHeader)
                .accept(JSON)
                .pathParam("id", userId)
                .when()
                .delete(CONTEXT_PATH + "/users/{id}")
                .then()
                .statusCode(200)
                .contentType(JSON)
                .extract()
                .response();

        String operationResult = response.jsonPath().getString("operationResult");
        assertEquals("SUCCESS", operationResult);

    }
}
